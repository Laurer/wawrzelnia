interface IAppConfig {
    apiUrl: string
}

const config : IAppConfig = window['appConfig'];

export default config;
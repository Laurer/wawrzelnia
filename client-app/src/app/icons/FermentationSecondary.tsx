const FermentationSecondary = ({ className }) => {
  return (
    <div className={`round-icon ${className}`}>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 150 150">
        <g>
          <g>
            <path d="M70.79,123.77,44.16,109.36a4.55,4.55,0,0,1-5.31-6.78,4.46,4.46,0,0,1,1.56-1.47l2.75-22.29,1.18,22a4.45,4.45,0,0,1,.81.42A4.53,4.53,0,0,1,47.24,105Z" />
            <rect x="41.13" y="70.11" width="3.51" height="5.91" />
            <rect x="41.13" y="132.97" width="3.51" height="5.91" />
            <rect x="72.47" y="102.74" width="5.91" height="3.51" />
            <rect x="10.18" y="102.74" width="5.91" height="3.51" />
            <rect
              x="19.63"
              y="79.87"
              width="3.51"
              height="5.91"
              transform="translate(-52.3 39.38) rotate(-45)"
            />
            <rect
              x="64.07"
              y="124.32"
              width="3.51"
              height="5.91"
              transform="translate(-70.72 83.82) rotate(-45)"
            />
            <rect
              x="19.21"
              y="124.74"
              width="5.91"
              height="3.51"
              transform="translate(-82.95 52.72) rotate(-45)"
            />
            <rect
              x="63.66"
              y="80.29"
              width="5.91"
              height="3.51"
              transform="translate(-38.5 71.16) rotate(-45.01)"
            />
          </g>
          <path d="M147.45,11.5c-65.83-24.25-122.15-.95-124.51.06l-2.4,1L26,64.63a44,44,0,0,0,8.7,83.06L35,150H138.2L150,12.42ZM7.08,104.72A36.92,36.92,0,1,1,44,141.65a38.42,38.42,0,0,1-5.56-.41A37.14,37.14,0,0,1,7.08,104.72ZM131.75,143H65.6A43.95,43.95,0,0,0,32.81,62.22l-2-19.34c10.15-3.56,51.94-15.78,109.5.24Zm9.14-107C84.17,20.53,43.14,31.23,30,35.67l-2-18.56c11.18-4,59.74-19.08,114.42.07Z" />
        </g>
      </svg>
    </div>
  );
};

export default FermentationSecondary;

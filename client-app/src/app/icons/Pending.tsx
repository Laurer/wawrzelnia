const Pending = () => {
  return (
    <div className="round-icon">
      <svg
        viewBox="0 0 100 100"
        y="0"
        x="0"
        xmlns="http://www.w3.org/2000/svg"
        id="pending-icon"
        version="1.1"
      >
        <g>
          <path
            d="M84 85.8h-7.9V74.5c0-6.6-4.2-12.8-11.3-16.6-3.2-1.7-5-4.3-5-7.1v-1.7c0-2.8 1.9-5.4 5.1-7.1 7.1-3.8 11.3-9.9 11.3-16.6V14.2H84c1 0 1.8-.8 1.8-1.8s-.8-1.6-1.8-1.6H16c-1 0-1.8.8-1.8 1.8s.8 1.8 1.8 1.8h7.9v11.2c0 6.6 4.2 12.8 11.3 16.6 3.2 1.7 5.1 4.3 5.1 7.1V51c0 2.8-1.9 5.4-5.1 7.1-7.1 3.8-11.3 9.9-11.3 16.6v11.2H16c-1 0-1.8.8-1.8 1.8s.8 1.8 1.8 1.8h68c1 0 1.8-.8 1.8-1.8s-.8-1.9-1.8-1.9zM27.3 74.5c0-5.3 3.5-10.3 9.4-13.5 4.3-2.3 6.9-6.1 6.9-10.2v-1.7c0-4.1-2.6-7.9-6.9-10.2-5.9-3.1-9.4-8.2-9.4-13.5V14.3h45.4v11.2c0 5.3-3.5 10.3-9.4 13.5-4.3 2.3-6.9 6.1-6.9 10.2v1.7c0 4.1 2.6 7.9 6.9 10.2 5.9 3.1 9.4 8.2 9.4 13.5v11.2H27.3V74.5z"
          />
          <circle
            strokeMiterlimit="10"
            strokeLinecap="round"
            strokeWidth="3.5"
            stroke="#000"
            fill="#000"
            r="17.5"
            cy="50"
            cx="50"
          />
		  <path
            d="M50 39.3V50"
            strokeMiterlimit="10"
            strokeLinecap="round"
            strokeWidth="3.5"
            stroke="#96fd00"
            fill="none"
          />
          <path 
            d="M57 54.1L50 50"
            strokeMiterlimit="10"
            strokeLinecap="round"
            strokeWidth="3.5"
            stroke="#96fd00"
            fill="none"
          />
        </g>
      </svg>
    </div>
  );
};

export default Pending;

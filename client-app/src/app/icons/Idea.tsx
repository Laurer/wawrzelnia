const Idea = ({ className }) => {
  return (
    <div className={`round-icon ${className}`}>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 150 150">
        <g>
          <path d="M111.87,61.9V33.36H38.12V61.9c-.07,1.47-.24,11.74,7.37,33.13s7.74,51.27,7.74,51.57V150H96.76v-3.4c0-.3.09-30.09,7.74-51.57S111.94,63.37,111.87,61.9ZM44.92,40.16h60.15V56.83H44.91ZM98.1,92.75c-6.56,18.42-7.83,41.78-8.08,50.45H60c-.24-8.67-1.51-32-8.07-50.45-5.88-16.5-6.84-25.84-7-29.12h60.15C104.93,66.94,104,76.26,98.1,92.75Z" />
          <rect x="71.15" y="0.01" width="7.78" height="20.65" />
          <rect x="129.35" y="56.24" width="20.65" height="7.78" />
          <rect
            x="114.25"
            y="12.41"
            width="20.65"
            height="7.78"
            transform="translate(24.96 92.86) rotate(-45)"
          />
          <rect
            x="120.69"
            y="93.64"
            width="7.78"
            height="20.65"
            transform="translate(-37.03 118.54) rotate(-45)"
          />
          <rect y="56.24" width="20.65" height="7.78" />
          <rect
            x="21.53"
            y="5.98"
            width="7.78"
            height="20.65"
            transform="translate(-4.08 22.75) rotate(-45)"
          />
          <rect
            x="15.1"
            y="100.07"
            width="20.65"
            height="7.78"
            transform="translate(-66.07 48.43) rotate(-45)"
          />
        </g>
      </svg>
    </div>
  );
};

export default Idea;

import React, { Fragment, useContext, useEffect } from "react";
import {
  Route,
  RouteComponentProps,
  Switch,
  withRouter,
} from "react-router-dom";
import { observer } from "mobx-react-lite";
import "./App.scss";
import BatchStore from "./stores/batchStore";
import Home from "./components/home/Home";
import Footer from "./components/Footer";
import NavBar from "./components/NavBar";
import NotFound from "./components/NotFound";
import BatchDetails from "./components/batches/BatchDetails";
import BatchesDashboard from "./components/batches/BatchesDashboard";
import Contact from "./components/contact/Contact";
import Process from "./components/process/Process";
import Pending from "./components/pending/Pending";

const App: React.FC<RouteComponentProps> = () => {
  const batchStore = useContext(BatchStore);
  useEffect(() => {
    batchStore.load();
  }, [batchStore]);

  return (
    <Fragment>
      <Route exact path="/" component={Home} />
      <Route
        exact
        path="/(.+)"
        component={() => (
          <Fragment>
            <NavBar className="static" />
            <Switch>
              <Route exact path="/batches" component={BatchesDashboard} />
              <Route path="/batches/:id" component={BatchDetails} />
              <Route exact path="/contact" component={Contact} />
              <Route exact path="/process" component={Process} />
              <Route exact path="/pending" component={Pending} />
              <Route component={NotFound} />
            </Switch>
          </Fragment>
        )}
      />
      <Footer />
    </Fragment>
  );
};

export default withRouter(observer(App));

export function formatDate(dateTime: string): string {
  const dateParts = dateTime.split("T")[0].split("-");
  return `${dateParts[2]}/${dateParts[1]}/${dateParts[0]}`;
}

export function getYearFromDate(dateTime: string): number {
  const date = new Date(dateTime);
  return date.getFullYear();
}

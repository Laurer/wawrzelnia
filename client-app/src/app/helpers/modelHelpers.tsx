import { IBatch } from "../../api/models/batch";

const labelPlaceholder = "/img/labels/0_label.png";
const bottlePlaceholder = "/img/bottles/0_cut.png";

export function getLabel(batch: IBatch): string {
    return batch.gallery?.label ?? labelPlaceholder;
}

export function getBottle(batch: IBatch): string {
    return batch.gallery?.bottle ?? bottlePlaceholder;
}

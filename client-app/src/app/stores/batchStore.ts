import { BrewStage } from "./../../api/models/brewStage";
import { IBatch } from "./../../api/models/batch";
import client from "../../api/client";
import { action, makeObservable, observable, runInAction } from "mobx";
import { createContext } from "react";
import { IStats } from "../../api/models/stats";
import { formatDate, getYearFromDate } from "../helpers/stringHelpers";
import _ from "lodash";

class BatchStore {
  @observable batches?: IBatch[] = undefined;
  @observable stats?: IStats = undefined;
  @observable isLoading = false;
  @observable loadingFailed = false;

  constructor() {
    makeObservable(this);
  }

  @action load = async () => {
    this.isLoading = true;
    try {
      this.batches = await client.Batches.getAll();
      runInAction(async () => {
        this.stats = await client.Batches.getStats();
      });
    } catch (error) {
      this.loadingFailed = true;
    } finally {
      runInAction(() => {
        this.isLoading = false;
      });
    }
  };

  getBatchesByYear(): [string, IBatch[]][] | undefined {
    if (!this.batches) {
      return undefined;
    }

    const sortedBatches = [...this.batches].sort(
      (a, b) => Date.parse(b.process.brewingDate) - Date.parse(a.process.brewingDate)
    );

    // TODO: Probably can be improved with lodash

    return Object.entries(
      sortedBatches.reduce((batches, batch) => {
        const date = getYearFromDate(batch.process.brewingDate);
        batches[date] = batches[date] ? [...batches[date], batch] : [batch];
        return batches;
      }, {} as { [key: string]: IBatch[] })
    ).sort((a, b) => Number.parseInt(b[0]) - Number.parseInt(a[0]));
  }

  getBatchesByStage(): [BrewStage, IBatch[]][] | undefined {
    if (!this.batches) {
      return undefined;
    }

    var grouped = _.groupBy(this.batches, "stage");

    // Add empty groups (TODO: This probably can be improved)
    Object.keys(BrewStage)
      .map((val) => Number(val))
      .filter((val) => !isNaN(val))
      .forEach((stage) => {
        if (!grouped[stage]) {
          grouped[stage] = [];
        }
      });
    return grouped;
  }

  getNextOpenning(): string | undefined {
    if (!this.batches) {
      return undefined;
    }

    const opennings = this.batches
      .map((batch) => batch.process.openningDate)
      .filter((date) => date && Date.parse(date) > Date.now());
    return formatDate(opennings[0]);
  }

  getBatch(batchId: number): IBatch | undefined {
    return this.batches?.filter((b) => b.batchNo === batchId)[0];
  }

  getNext(batch: IBatch): IBatch | undefined {
    return this.batches?.filter((b) => b.batchNo === batch.batchNo + 1)[0];
  }

  getPrevious(batch: IBatch): IBatch | undefined {
    return this.batches?.filter((b) => b.batchNo === batch.batchNo - 1)[0];
  }
}

export default createContext(new BatchStore());

import { useEffect } from "react";
import { useLocation } from "react-router-dom";
import * as Scroll from 'react-scroll';

export default function ScrollToTop() {
  const { pathname } = useLocation();

  useEffect(() => {
    Scroll.animateScroll.scrollToTop();
  }, [pathname]);

  return null;
}

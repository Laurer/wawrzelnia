import React, { Fragment, useContext } from "react";
import BatchStore from "./stores/batchStore";
import Loader from "./components/Loader";
import Error from "./components/Error";
import { observer } from "mobx-react-lite";

const ValidateBatches: React.FC = ({ children }) => {
  const batchStore = useContext(BatchStore);

  if (batchStore.isLoading) {
    return <Loader showBar={false} message="Momencik..." />;
  } else if (batchStore.loadingFailed) {
    return <Error message="Ups... Coś poszło nie tak. :(" />;
  }

  return <Fragment>{children}</Fragment>;
};

export default observer(ValidateBatches);

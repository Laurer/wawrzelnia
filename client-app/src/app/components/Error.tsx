import React from "react";
import "./Error.scss";
import ErrorIcon from "../icons/Error";

const Error: React.FC<{ message?: string }> = (props) => {
  return (
    <div id="error-container">
      <ErrorIcon />
      <h4 className="message">{props.message}</h4>
    </div>
  );
};

export default Error;

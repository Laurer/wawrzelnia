import React, { Fragment, useState } from "react";
import NavBar from "../NavBar";
import Hero from "./Hero";
import Process from "./Process";
import Pending from "./Pending";
import History from "./History";
import { observer } from "mobx-react-lite";
import { Waypoint } from "react-waypoint";
import ValidateBatches from "../../ValidateBatches";

const Home: React.FC = () => {
  const [onTop, setOnTop] = useState(true);

  return (
    <Fragment>
      <NavBar className={`fixed ${onTop ? "hidden" : ""}`} />
      <Waypoint
        onEnter={() => setOnTop(true)}
        onLeave={() => setOnTop(false)}
      ></Waypoint>
      <Hero />
      <ValidateBatches>
        <History />
        <Pending />
        <Process />
      </ValidateBatches>
    </Fragment>
  );
};

export default observer(Home);

import React, { useContext } from "react";
import { BrewStage } from "../../../api/models/brewStage";
import BatchStore from "../../stores/batchStore";
import "./Pending.scss";
import ValidateBatches from "../../ValidateBatches";
import PendingIcon from "../../icons/Pending";
import { Link } from "react-router-dom";

const Pending: React.FC = () => {
  const batchStore = useContext(BatchStore);

  if (batchStore.isLoading || !batchStore.batches) {
    return null;
  }

  const batches = batchStore.getBatchesByStage();
  if (!batches) {
    return null;
  }

  return (
    <ValidateBatches>
      <section id="home-pending" className="main-section">
        <div className="container">
          <h2 className="section-title">Warzy się</h2>
          <div className="pending-content">
            <div className="intro">
              <PendingIcon />
              <p>
                <span>Warzenie piwa</span> to w większości planowanie i
                oczekiwanie na rezultaty.
              </p>
              <p>
                Dlatego, aby <span>jutro</span> móc cieszyć się pysznym trunkiem, trzeba o niego zadbać już <span>dzisiaj</span>!
              </p>
            </div>
            <div className="separator" />
            <div className="stats">
              <div className="stats-content">
                <p className="name">Warki w trakcie fermentacji</p>
                <h3 className="value">
                  {batches[BrewStage.Brewing].length}
                </h3>
                <p className="name">
                  Warki leżakujące
                  <br />
                  <span>(dojrzewają i czekają na swoją premierę)</span>
                </p>
                <h3 className="value">{batches[BrewStage.Maturing].length}</h3>
                <p className="name">
                  Piwa gotowe do picia <br />
                  <span>(a mimo to jeszcze nie wypite)</span>
                </p>
                <h3 className="value">{batches[BrewStage.Opened].length}</h3>
                <p className="name">Najbliższa dekapslacja</p>
                <h3 className="value" style={{ fontSize: "1.5rem" }}>
                  {batchStore.getNextOpenning()}
                </h3>
              </div>
              <Link to="pending" className="btn">
                szczegóły
              </Link>
            </div>
          </div>
        </div>
      </section>
    </ValidateBatches>
  );
};

export default Pending;

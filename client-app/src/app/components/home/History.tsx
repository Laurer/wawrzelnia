import React, { useContext } from "react";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import BatchStore from "../../stores/batchStore";
import "./History.scss";
import "@splidejs/splide/dist/css/themes/splide-sea-green.min.css";
import { observer } from "mobx-react-lite";
import { Link } from "react-router-dom";
import { formatDate } from "../../helpers/stringHelpers";
import { getLabel } from "../../helpers/modelHelpers";

const History: React.FC = () => {
  const batchStore = useContext(BatchStore);

  if (batchStore.isLoading || !batchStore.batches) {
    return null;
  }

  let allBatches = batchStore.batches.filter((batch) => batch.stage >= 4);
  let batches = allBatches.slice(Math.max(allBatches.length - 5, 0));

  return (
    <section id="history" className="main-section">
      <div className="container">
        <h2 className="section-title">Uwarzone</h2>
        <h4>
          Łącznie powstało już <span>{allBatches.length}</span> warek! Każda z
          nich to inny smak, ale przede wszystkim - osobna historia.
        </h4>
        <Splide
          id="brew-carousel"
          options={{
            type: "loop",
            gap: "2rem",
            padding: {
              right: "2rem",
              left: "2rem",
            },
            start: batches.length,
            lazyLoad: "nearby",
          }}
        >
          <SplideSlide className="slide-item">
            <div className="older-items">
              <h4>Chcesz zobaczyć wcześniejsze warki?</h4>
              <a href="batches" className="btn">
                Pokaż wszystkie
              </a>
            </div>
          </SplideSlide>
          {batches.map((batch) => {
            return (
              <SplideSlide key={batch.batchNo}>
                <Link className="slide-item" to={`batches/${batch.batchNo}`}>
                  <div className="brew-image dim-right10">
                    <img
                      className="bottle-img"
                      data-splide-lazy={
                        batch.gallery.profile ? batch.gallery.profile : getLabel(batch)
                      }
                      alt=""
                    />
                    <div className="brew-no">
                      <h3>#{batch.batchNo}</h3>
                    </div>
                    {batch.gallery.profile && (
                      <div className="brew-dates">
                        <div className="brew-date brewing">
                          <h3 className="name">Warzenie</h3>
                          <h4 className="value">
                            {formatDate(batch.process.brewingDate)}
                          </h4>
                        </div>
                        <div className="brew-date">
                          <h3 className="name">Rozlew</h3>
                          <h4 className="value">
                            {formatDate(batch.process.bottlingDate)}
                          </h4>
                        </div>
                      </div>
                    )}
                  </div>
                  <div className="brew-details">
                    <h2 className="brew-name">{batch.name}</h2>
                    <h3 className="brew-style">{batch.style}</h3>
                    <p className="brew-description">{batch.description}</p>
                    <div className="values">
                      <div className="val-group">
                        <h3>ALK</h3>
                        <p>{batch.alcohol}%</p>
                      </div>
                      <div className="line-v"></div>
                      <div className="val-group">
                        <h3>IBU</h3>
                        <p>{batch.ibu}</p>
                      </div>
                      <div className="line-v"></div>
                      <div className="val-group">
                        <h3>BLG</h3>
                        <p>{batch.blg}</p>
                      </div>
                    </div>
                    <div className="btn"> szczegóły</div>
                  </div>
                </Link>
              </SplideSlide>
            );
          })}
        </Splide>
      </div>
    </section>
  );
};

export default observer(History);

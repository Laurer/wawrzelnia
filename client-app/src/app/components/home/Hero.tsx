import React from "react";
import "./Hero.scss";

const Hero: React.FC = () => {
  return (
    <div>
      <header
        id="hero"
        className="dim"
        style={{ background: `url("./img/hero.jpg") no-repeat top/cover` }}
      >
        <div className="hero-content container">
          <div className="logo">
            <img className="logo-img" src="./img/logo-g.png" alt="" />
          </div>
          <div className="keynote">
            <div>
              <h1 className="title">WAWRZELNIA</h1>
              <div className="line-h"></div>
            </div>
            <h2 className="subtitle">jednoosobowa działalność piwowarska</h2>
            <p className="description">
              <span>Warzyć warto, ale ważne jak!</span> W czasach, gdy kraft
              krafta w multitapie pogania, piwa to coś więcej niż tylko
              spienione napoje. Dlatego moje warki to odważne eksperymenty,
              błyskotliwi i wnikliwi obserwatorzy codziennych wydarzeń.
            </p>
          </div>
        </div>
      </header>
    </div>
  );
};

export default Hero;

import { observer } from "mobx-react-lite";
import React, { useContext } from "react";
import BatchStore from "../../stores/batchStore";
import FullProcess from "../process/Process";
import "./Process.scss";

const Process: React.FC = () => {
  const batchStore = useContext(BatchStore);
  const { stats } = batchStore;

  if (batchStore.isLoading || !stats) {
    return null;
  }

  return <FullProcess />;
};

export default observer(Process);

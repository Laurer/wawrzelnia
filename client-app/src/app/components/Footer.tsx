import React from "react";
import "./Footer.scss";
import packageJson from "../../../package.json";

const Footer: React.FC = () => {
  return (
    <footer id="footer">
      <div className="container">
        <div className="footer-content">
          <img
            className="logo-wasoft"
            src={process.env.PUBLIC_URL + "/img/wasoft.png"}
            alt=""
          />
          <div className="main-content">
            <p>Autor: <a href="http://www.wawrzynek.info">Paweł Wawrzynek</a></p>
            <p>Kontakt: <a href="mailto:wawrzelnia@gmail.com">mail</a> / <a href="/contact">formularz</a></p>
            <hr></hr>
            <p>v.{packageJson.version} | {new Date().getFullYear()} &copy; <a href="http://www.wasoft.pl">WaSoft</a> All
              rights reserved
            </p>
          </div>
          <img
            className="logo-wasoft"
            src={process.env.PUBLIC_URL + "/img/wasoft.png"}
            alt=""
          />
        </div>
      </div>
    </footer>
  );
};

export default Footer;

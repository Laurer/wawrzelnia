import { runInAction } from "mobx";
import React, { useState } from "react";
import Swal from "sweetalert2";
import client from "../../../api/client";
import { IContactForm } from "../../../api/models/contactForm";
import Opinion from "../../icons/Opinion";
import ValidateBatches from "../../ValidateBatches";
import "./Contact.scss";

const Contact: React.FC = () => {
  const [input, setInput] = useState<IContactForm>({
    name: "",
    returnAddress: "",
    message: "",
  });

  const handleInputChange = (event) => {
    event.persist();
    setInput((state) => ({
      ...state,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    let result = await client.Contact.sendContactForm(input);
    runInAction(() => {
      Swal.fire({
        icon: result === true ? "success" : "error",
        title:
          result === true
            ? "Twoja wiadomość została wysłana!"
            : "Nie udało się wysłać Twojej wiadomości. Spróbuj później.",
        showConfirmButton: false,
        timer: 3000,
      });

      if (result === true) {
        setInput({
          name: "",
          returnAddress: "",
          message: "",
        });
      }
    });
  };

  return (
    <ValidateBatches>
      <section id="contact" className="main-section">
        <div className="container">
          <h2 className="section-title">Daj znać</h2>
          <div className="contact-content">
            <div className="contact-info">
              <Opinion />
              <p>
                Masz <span>pomysł</span> na nowe piwo?
              </p>
              <p>
                A może chociaż na <span>nazwę</span> lub <span>tematykę</span>{" "}
                kolejnej warki?
              </p>
              <p>
                Chcesz <span>spróbować</span> któregoś z wyrobów?
              </p>
              <p>
                No dobra... Cokolwiek by to nie było - <span>wal śmiało</span>!
              </p>
            </div>
            <form id="contact-form" onSubmit={handleSubmit}>
              <input
                type="text"
                name="name"
                placeholder="Ktoś Ty?"
                required
                onChange={handleInputChange}
                value={input.name}
              />
              <input
                type="email"
                name="returnAddress"
                placeholder="Gdzie Ci odpisać?"
                required
                onChange={handleInputChange}
                value={input.returnAddress}
              />
              <textarea
                name="message"
                placeholder="Co słychać?"
                required
                onChange={handleInputChange}
                value={input.message}
              ></textarea>
              <input type="submit" value="Ślij!" />
            </form>
          </div>
        </div>
      </section>
    </ValidateBatches>
  );
};

export default Contact;

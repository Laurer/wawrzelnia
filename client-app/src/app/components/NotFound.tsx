import React from "react";
import "./NotFound.scss";
import Error from "../icons/Error";

const NotFound: React.FC = () => {
  return (
    <div id="not-found">
      <h2 className="title">W tył zwrot!</h2>
      <Error />
      <div className="message">
        <h4>Pod tym adresem nie mieszka żadna strona.</h4>
        <h4>
          Może lepiej wrócić na <a href="/">główną</a>?
        </h4>
      </div>
    </div>
  );
};

export default NotFound;

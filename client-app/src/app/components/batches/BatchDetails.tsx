import React, { Fragment, useContext } from "react";
import "./BatchDetails.scss";
import BatchStore from "../../stores/batchStore";
import { observer } from "mobx-react-lite";
import { Link, RouteComponentProps } from "react-router-dom";
import ValidateBatches from "../../ValidateBatches";
import NotFound from "../NotFound";
import Brewing from "../../icons/Brewing";
import Packaging from "../../icons/Packaging";
import Premiere from "../../icons/Premiere";
import Bottle from "../../icons/Bottle";
import { formatDate } from "../../helpers/stringHelpers";
import { getBottle, getLabel } from "../../helpers/modelHelpers";

const BatchDetails: React.FC<
  RouteComponentProps<{
    id: string;
  }>
> = ({ match }) => {
  const batchStore = useContext(BatchStore);

  const batchId = parseInt(match.params.id, 10);
  const batch = batchStore.getBatch(batchId);

  // TODO: It contradicts the idea of ValidateBatches as "Not Found" gets shown, if loading in progress or failed
  if (!batch) {
    return <NotFound />;
  }

  const previous = batchStore.getPrevious(batch);
  const next = batchStore.getNext(batch);

  return (
    <ValidateBatches>
      <div id="batch-details">
        <div className="container">
          <div className="return">
            <Link to="/batches" className="btn">
              Powrót
            </Link>
          </div>
          <div className="main-info">
            <div className="label">
              <img src={getLabel(batch)} alt="" />
            </div>
            <div className="bottle">
              <img src={getBottle(batch)} alt="" />
            </div>
            <div className="info">
              <div className="batch-no round-icon">
                <h3>#{batch.batchNo}</h3>
              </div>
              <h2>{batch.name}</h2>
              <div className="line-h" />
              <p>{batch.story}</p>
              <div className="line-h" />
            </div>
          </div>
          <div className="details">
            <div className="values">
              <div className="info-item">
                <div className="info-desc">
                  <h3>ALK.</h3>
                </div>
                <div className="info-value round-icon">
                  <h3>{batch.alcohol}%</h3>
                </div>
              </div>
              <div className="info-item">
                <div className="info-desc">
                  <h3>BLG</h3>
                </div>
                <div className="info-value round-icon">
                  <h3>{batch.blg}</h3>
                </div>
              </div>
              <div className="info-item">
                <div className="info-desc">
                  <h3>IBU</h3>
                </div>
                <div className="info-value round-icon">
                  <h3>{batch.ibu}</h3>
                </div>
              </div>
            </div>
            <div className="description">
              <h3 className="batch-style">{batch.styleFull}</h3>
              <div className="line-h" />
              <p>{batch.description}</p>
            </div>
            <div className="dates">
              <div className="info-item">
                <div className="info-value">
                  <Brewing className=""></Brewing>
                </div>
                <div className="info-desc">
                  <h3>{formatDate(batch.process.brewingDate)}</h3>
                </div>
              </div>
              <div className="info-item">
                <div className="info-value">
                  <Packaging />
                </div>
                <div className="info-desc">
                  <h3>{formatDate(batch.process.bottlingDate)}</h3>
                </div>
              </div>
              <div className="info-item">
                <div className="info-value">
                  <Premiere />
                </div>
                <div className="info-desc">
                  <h3>{formatDate(batch.process.openningDate)}</h3>
                </div>
              </div>
            </div>
          </div>
          <div className="navigation">
            {!previous ? (
              ""
            ) : (
              <Fragment>
                <Link to={`${previous.batchNo}`} className="prev-link">
                  <Bottle />
                </Link>
                <h3 className="prev-name">{previous.name}</h3>
              </Fragment>
            )}
            {next ? (
              <Fragment>
                <h3 className="next-name">{next.name}</h3>
                <Link to={`${next.batchNo}`} className="next-link">
                  <Bottle />
                </Link>
              </Fragment>
            ) : (
              ""
            )}
          </div>
        </div>
      </div>
    </ValidateBatches>
  );
};

export default observer(BatchDetails);

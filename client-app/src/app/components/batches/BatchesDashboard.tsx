import React, { useContext } from "react";
import "./BatchesDashboard.scss";
import { observer } from "mobx-react-lite";
import ValidateBatches from "../../ValidateBatches";
import BatchStore from "../../stores/batchStore";
import { Link } from "react-router-dom";
import { getLabel } from "../../helpers/modelHelpers";

const BatchesDashboard: React.FC = () => {
  const batchStore = useContext(BatchStore);

  // TODO: It contradicts the idea of ValidateBatches as "Not Found" gets shown, if loading in progress or failed
  if (!batchStore.batches) {
    return null;
  }

  const batchYears = batchStore.getBatchesByYear();

  return (
    <ValidateBatches>
      <div id="batches-dashboard">
        <div className="container">
          <h2 className="section-title">Uwarzone</h2>
          <h4>Cała przygoda rozpoczęła się w Boże Narodzenie roku 2018...</h4>
          <h4>
            Od tego czasu powstało już <span>{batchStore.batches.length}</span>{" "}
            warek. Złośliwi twierdzą, że to o co najmniej{" "}
            {Math.floor(batchStore.batches.length * 0.75)} za dużo, ale któż
            słuchałby takich głosów...
          </h4>
          <div className="batches-years">
            {batchYears?.map((group) => {
              let batches = group[1];
              return (
                <div className="batches-year" key={group[0]}>
                  <div className="timeline-point"></div>
                  <h3 className="year-title">{group[0]}</h3>
                  <div className="batches-grid">
                    {batches?.map((batch) => {
                      return (
                        <div key={batch.batchNo} className="batch-item">
                          <Link
                            to={`batches/${batch.batchNo}`}
                            className="info-link"
                          >
                            <img src={getLabel(batch)} alt="" />
                            <div className="info-button">
                              <div className="btn">Wincyj!</div>
                            </div>
                          </Link>
                        </div>
                      );
                    })}
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </ValidateBatches>
  );
};

export default observer(BatchesDashboard);

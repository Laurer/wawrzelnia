import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import { Container, Menu } from "semantic-ui-react";
import "./NavBar.scss";

const NavBar: React.FC<{ className }> = (props) => {
  const [showPanel, setShowPanel] = useState(false);

  return (
    <Menu className={props.className}>
      <Container>
        <Menu.Item className="logo" as={NavLink} exact to="/">
          <img
            className="logo-img"
            src={process.env.PUBLIC_URL + "/img/logo-g-thick.png"}
            alt=""
          />
        </Menu.Item>
        <div id="main-nav">
          <Menu.Item name="WAWRZELNIA" as={NavLink} exact to="/" />
          <Menu.Item name="Uwarzone" as={NavLink} to="/batches" />
          <Menu.Item as={NavLink} exact to="/pending">
            Warzy się
          </Menu.Item>
          {/* <Menu.Item as={NavLink} exact to="/process">
            Piwny proces
          </Menu.Item> */}
          <Menu.Item as={NavLink} exact to="/contact">
            Daj znać
          </Menu.Item>
        </div>
        <div id="mobile-nav">
          <input
            type="checkbox"
            className="toggler"
            checked={showPanel}
            onChange={() => setShowPanel(!showPanel)}
          />
          <div className="hamburger">
            <div></div>
          </div>
        </div>
        <div id="side-panel" className={showPanel ? "" : " hidden"}>
          <Menu.Item
            name="WAWRZELNIA"
            as={NavLink}
            exact
            to="/"
            onClick={() => setShowPanel(false)}
          />
          <Menu.Item
            name="Uwarzone"
            as={NavLink}
            to="/batches"
            onClick={() => setShowPanel(false)}
          />
          <Menu.Item
            as={NavLink}
            exact
            to="/pending"
            onClick={() => setShowPanel(false)}
          >
            Warzy się
          </Menu.Item>
          {/* <Menu.Item
            as={NavLink}
            exact
            to="/process"
            onClick={() => setShowPanel(false)}
          >
            Piwny proces
          </Menu.Item> */}
          <Menu.Item
            as={NavLink}
            exact
            to="/contact"
            onClick={() => setShowPanel(false)}
          >
            Daj znać
          </Menu.Item>
        </div>
      </Container>
    </Menu>
  );
};

export default NavBar;

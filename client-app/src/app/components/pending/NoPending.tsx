import React from "react";
import "./NoPending.scss";

const NoPending: React.FC = () => {
  return (
    <div id="no-pending-container">
      <div className="header">
        <img className="icon" src="/img/spilled.png" alt=""/>
        <h2>Ratunku, posucha!</h2>
      </div>
      <div className="message">
        <p>
          Stała się rzecz niebywała! Wygląda na to, że aktualnie nie powstaje
          żadne nowe piwo... Jeżeli Ciebie to też bulwersuje, nie wahaj się ani
          chwili - działaj!
        </p>
        <p>
          Zamknij oczy, zastanów się na jakie piwko masz ochotę i, bez zbędnej
          zwłoki, <a href="/contact">daj mi znać</a>. Raz, dwa, weźmiemy się do
          warzenia.
        </p>
      </div>
    </div>
  );
};

export default NoPending;

import React, { useContext } from "react";
import { IBatch } from "../../../api/models/batch";
import { BrewStage } from "../../../api/models/brewStage";
import Idea from "../../icons/Idea";
import Brewing from "../../icons/Brewing";
import BatchStore from "../../stores/batchStore";
import "./Pending.scss";
import Maturing from "../../icons/Maturing";
import { formatDate } from "../../helpers/stringHelpers";
import { getLabel } from "../../helpers/modelHelpers";
import NoPending from "./NoPending";

const getStageClass = (batch: IBatch, stage: BrewStage): string => {
  if (batch.stage > stage) {
    return "done";
  } else if (batch.stage < stage) {
    return "todo";
  }
  return "active";
};

const getStageName = (stage: number): string => {
  switch (stage) {
    case BrewStage.Design:
      return "W planie";
    case BrewStage.Brewing:
      return "Warzenie";
    case BrewStage.Maturing:
      return "Leżakowanie";
    case BrewStage.Opened:
      return "Picie";
    default:
      return "";
  }
};

const Pending: React.FC = () => {
  const batchStore = useContext(BatchStore);

  if (batchStore.isLoading || !batchStore.batches) {
    return null;
  }

  const batches = batchStore.batches.filter((b) => b.stage < BrewStage.Opened);

  if (batches.length === 0) {
    return (
      <div id="pending">
        <div className="container">
          <NoPending />
        </div>
      </div>
    );
  }

  return (
    <div id="pending">
      <div className="container">
        <h2 className="section-title">Warzy się</h2>
        {batches.map((batch) => {
          return (
            <div className="brew" key={batch.batchNo}>
              <div className="brew-label">
                <img src={getLabel(batch)} alt="" />
                <h3 className="brew-name">{batch.name}</h3>
              </div>
              <div className="brew-info">
                <div className="stages">
                  <Idea className={getStageClass(batch, BrewStage.Design)} />
                  <Brewing
                    className={getStageClass(batch, BrewStage.Brewing)}
                  />
                  <Maturing
                    className={getStageClass(batch, BrewStage.Maturing)}
                  />
                  <div className="stage-name">
                    <h4>{getStageName(batch.stage)}</h4>
                  </div>
                </div>
                <div className="brew-info-content">
                  <h4 className="brew-info-title">
                    Ważą się losy warki{" "}
                    <span className="brew-no">#{batch.batchNo}</span>!
                  </h4>
                  <h4 className="brew-info-date">
                    Dekapslacja:{" "}
                    <span className="brew-no">
                      {formatDate(batch.process.openningDate)}
                    </span>
                  </h4>
                  <p>{batch.description}</p>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Pending;

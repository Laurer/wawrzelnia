import { observer } from "mobx-react-lite";
import React, { useContext } from "react";
import FermentationPrimary from "../../icons/FermentationPrimary";
import FermentationSecondary from "../../icons/FermentationSecondary";
import Hopping from "../../icons/Hopping";
import Mashing from "../../icons/Mashing";
import Maturing from "../../icons/Maturing";
import Packaging from "../../icons/Packaging";
import BatchStore from "../../stores/batchStore";
import "./Process.scss";
import ProcessItem from "./ProcessItem";

const Process: React.FC = () => {
  const batchStore = useContext(BatchStore);
  const { stats } = batchStore;

  if (batchStore.isLoading || !stats) {
    return null;
  }

  return (
    <section id="process" className="main-section">
      <div className="container">
        <div className="stat stat-nozzle dim-in">
          <div className="nozzle-content">
            <h2 className="section-title">Piwny proces</h2>
            <div className="idea-icon">
              <img src="/img/idea.png" alt=""/>
            </div>
            <div className="pipe-start">
              <div className="link"></div>
            </div>
          </div>
        </div>
        <div className="stat stat-intro dim-in">
          <div className="line-h"></div>
          <p>Najpierw pomysł, projekt i myśl przewodnia.</p>
          <p>Później dużo czasu, miejsca i bałaganu.</p>
          <p>Dalej próba cierpliwości.</p>
          <p>Aż w końcu efekt - jakikolwiek by nie był.</p>
          <div className="line-h"></div>
        </div>
        <ProcessItem
          title="Zacieranie"
          subtitle="i wysładzanie"
          icon={<Mashing className="bi-mashing" />}
          value={stats.mashingHours}
          unit="h"
        />
        <ProcessItem
          title="Chmielenie"
          subtitle="(gotowanie)"
          icon={<Hopping className="bi-hop" />}
          value={stats.hoppingHours}
          unit="h"
        >
          <div className="link-top-left"></div>
          <div className="link-right-bottom"></div>
        </ProcessItem>
        <ProcessItem
          title="Fermentacja burzliwa"
          subtitle="(bulgotanie)"
          icon={<FermentationPrimary className="bi-fermentation-1" />}
          value={stats.primaryFermentationDays}
          unit="dni"
        >
          <div className="link-top-left"></div>
          <div className="link-left-bottom"></div>
        </ProcessItem>
        <ProcessItem
          title="Fermentacja cicha"
          subtitle="(odpoczywanie)"
          icon={<FermentationSecondary className="bi-fermentation-2" />}
          value={stats.secondaryFermentationDays}
          unit="dni"
        />
        <ProcessItem
          title="Rozlew"
          subtitle="(kapslowanie)"
          icon={<Packaging />}
          value={stats.bottlingHours}
          unit="h"
        >
          <div className="link-top-right"></div>
        </ProcessItem>
        <ProcessItem
          title="Leżakowanie"
          subtitle="(dojrzewanie)"
          icon={<Maturing className="" />}
          value={stats.maturingDays}
          unit="tyg."
        >
          <div className="link-top-right"></div>
          <div className="link-right-bottom"></div>
        </ProcessItem>

        <div className="stat stat-nozzle dim-out">
          <div className="nozzle-content">
            <div className="pipe-end">
              <div className="link"></div>
              <div className="link-top-right"></div>
            </div>
            <img className="glass-icon" src="./img/teku-thick.png" alt="" />
          </div>
        </div>
        <div className="stat stat-summary dim-out">
          <h3 className="summary-title">No i wyszło!</h3>
          <div className="summary-content">
            <h3 className="summary-value">{stats.batchesCount}</h3>
            <div className="line-v"></div>
            <h4 className="summary-desc">
              warek<span>lepszych, gorszych i nieudanych</span>
            </h4>
            <h3 className="summary-value">
              {stats.totalBottles}
            </h3>
            <div className="line-v"></div>
            <h4 className="summary-desc">
              butelek<span>nie licząc tych, które wybuchły</span>
            </h4>
            <h3 className="summary-value">{stats.totalLitres}</h3>
            <div className="line-v"></div>
            <h4 className="summary-desc">
              litrów<span>wypitych, podarowanych i wylanych</span>
            </h4>
          </div>
        </div>
      </div>
    </section>
  );
};

export default observer(Process);

import React from "react";
import CountUp from "react-countup";
import { Waypoint } from "react-waypoint";
import "./ProcessItem.scss";

export interface IProcessItemProps {
  icon?: JSX.Element;
  title: string;
  subtitle: string;
  value: number;
  unit: string;
}

const ProcessItem: React.FC<IProcessItemProps> = (props) => {
  return (
    <div className="stat stat-step">
      <div className="stat-step-content">
        {props.icon}        
        <h3 className="stat-name">
          {props.title} <span>{props.subtitle}</span>
        </h3>
        <div className="line-v"></div>
        <CountUp end={props.value} duration={2}>
          {({ countUpRef, start }) => (
            <Waypoint onEnter={start} bottomOffset="20%">
              <h3 className="stat-value">
                <span ref={countUpRef} />
                <span className="unit">{props.unit}</span>
              </h3>
            </Waypoint>
          )}
        </CountUp>
      </div>
      {props.children}
    </div>
  );
};

export default ProcessItem;

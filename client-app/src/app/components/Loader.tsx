import Bottle from "../icons/Bottle";
import "./Loader.scss";
// GIF downloaded from: https://www.uplabs.com/posts/loading-micro-animation and adapted to React with some styling changes

const Loader: React.FC<{ showBar: boolean; message?: string }> = (props) => {

  return (
    <div id="loader">
      {props.message ? <h4 className="message">{props.message}</h4> : ""}
      <div className="bottle-wrap">
        <div className="cap">
          <i></i>
          <i></i>
          <i></i>
          <i></i>
        </div>
        <Bottle />
        <Bottle />
        <Bottle />
        <Bottle />
      </div>
      {props.showBar ? <div className="bar"></div> : ""}
    </div>
  );
};

export default Loader;

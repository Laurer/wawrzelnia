export interface IContactForm {
    name: string,
    returnAddress: string,
    message: string
};
export interface IProcess {
    finalVolume?: number;
    finalBottles?: number;
    finalKegs?: number;
    wastedBottles?: number;

    mashingTime?: number;
    filtrationTime?: number;
    hoppingTime?: number;
    primaryFermentationTime?: number;
    secondaryFermentationTime?: number;
    bottlingTime?: number;
    labelingTime?: number;
    
    brewingDate: string;
    primaryFermentationDate: string;
    secondaryFermentationDate: string;
    bottlingDate: string;
    openningDate: string;
}
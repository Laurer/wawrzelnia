export enum BrewStage {
    Design,
    Brewing,
    Maturing,
    Opened,
    Finished
}
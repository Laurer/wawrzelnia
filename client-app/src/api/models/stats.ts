export interface IStats {
  batchesCount: number;

  averageAlcohol: number;
  averageIbu: number;
  averageBlg: number;

  totalLitres: number;
  totalBottles: number;
  totalKegs: number;
  totalWastedBottles: number;
  
  mashingHours: number;
  hoppingHours: number;
  primaryFermentationDays: number;
  secondaryFermentationDays: number;
  bottlingHours: number;
  maturingDays: number;    
}

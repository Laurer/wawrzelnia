export interface IGallery {
    bottle?: string;
    profile?: string;
    label?: string;
    opening?: string;
}
import { IProcess } from './process';
import { BrewStage } from "./brewStage";
import { IGallery } from "./gallery";

export interface IBatch {
  batchNo: number;
  name: string;
  stage: BrewStage;

  category: string;
  style: string;
  styleFull: string;

  description: string;
  story: string;

  alcohol: number;
  ibu: number;
  blg: number;

  process: IProcess;
  gallery: IGallery;
}
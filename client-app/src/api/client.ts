import { IContactForm } from "./models/contactForm";
import { IBatch } from "./models/batch";
import { IStats } from "./models/stats";
import axios, { AxiosResponse } from "axios";
import appConfig from '../appConfig';

axios.defaults.baseURL = appConfig.apiUrl;

const responseBody = (response: AxiosResponse) =>
  response ? response.data : [];

const requests = {
  get: (url: string) => axios.get(url).then(responseBody),
  post: (url: string, body: {}) => axios.post(url, body).then(responseBody),
  tryPost: (url: string, body: {}) =>
    axios
      .post(url, body)
      .then(() => true)
      .catch(() => false),
  put: (url: string, body: {}) => axios.put(url, body).then(responseBody),
  del: (url: string) => axios.delete(url).then(responseBody),
};

const Batches = {
  getAll: (): Promise<IBatch[]> => requests.get("/batches"),
  getStats: (): Promise<IStats> => requests.get("/batches/stats"),
};

const Contact = {
  sendContactForm: async (form: IContactForm): Promise<boolean> =>
    requests.tryPost("/contact", form),
};

export default {
  Batches,
  Contact,
};

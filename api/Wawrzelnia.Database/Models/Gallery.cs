﻿namespace Wawrzelnia.Database.Models
{
    public class Gallery
    {
        public int Id { get; set; }
        public int BatchId { get; set; }

        public string Bottle { get; set; }
        public string Profile { get; set; }
        public string Label { get; set; }
        public string Opening { get; set; }

        public virtual Batch Batch { get; set; }
    }
}

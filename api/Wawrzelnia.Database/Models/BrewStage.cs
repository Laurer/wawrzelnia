﻿namespace Wawrzelnia.Database.Models
{
    public enum BrewStage
    {
        Design = 1,
        Brewing = 2,
        Maturing = 3,
        Opened = 4,
        Finished = 5
    }
}

﻿namespace Wawrzelnia.Database.Models
{
    public class Batch
    {
        public int Id { get; set; }

        public int BatchNo { get; set; }
        public string Name { get; set; }
        public BrewStage Stage { get; set; }

        public string Category { get; set; }
        public string Style { get; set; }
        public string StyleFull { get; set; }

        public string Description { get; set; }
        public string Story { get; set; }

        public float Alcohol { get; set; }
        public int Ibu { get; set; }
        public float Blg { get; set; }

        public virtual ProcessData Process { get; set; }
        public virtual Gallery Gallery { get; set; }
    }
}

﻿using System;

namespace Wawrzelnia.Database.Models
{
    public class ProcessData
    {
        public int Id { get; set; }
        public int BatchId { get; set; }

        public float? FinalVolume { get; set; }
        public int? FinalBottles { get; set; }
        public int? WastedBottles { get; set; }

        public float? MashingTime { get; set; }
        public float? FiltrationTime { get; set; }
        public float? HoppingTime { get; set; }
        public float? PrimaryFermentationTime { get; set; }
        public float? SecondaryFermentationTime { get; set; }
        public float? BottlingTime { get; set; }
        public float? LabelingTime { get; set; }

        public DateTime? BrewingDate { get; set; }
        public DateTime? PrimaryFermentationDate { get; set; }
        public DateTime? SecondaryFermentationDate { get; set; }
        public DateTime? BottlingDate { get; set; }
        public DateTime? OpenningDate { get; set; }

        public virtual Batch Batch { get; set; }
    }
}

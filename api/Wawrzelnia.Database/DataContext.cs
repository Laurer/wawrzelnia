﻿using Microsoft.EntityFrameworkCore;
using Wawrzelnia.Database.Models;

namespace Wawrzelnia.Database
{
    public class DataContext : DbContext, IDataContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<Batch> Batches { get; set; }
        public DbSet<Gallery> Galleries { get; set; }
        public DbSet<ProcessData> ProcessData { get; set; }
    }
}

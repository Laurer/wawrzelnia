﻿using Microsoft.EntityFrameworkCore;
using Wawrzelnia.Database.Models;

namespace Wawrzelnia.Database
{
    public interface IDataContext
    {
        DbSet<Batch> Batches { get; set; }
        DbSet<Gallery> Galleries { get; set; }
        DbSet<ProcessData> ProcessData { get; set; }
    }
}

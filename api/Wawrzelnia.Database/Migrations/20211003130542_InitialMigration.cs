﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Wawrzelnia.Database.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Batches",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BatchNo = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Stage = table.Column<int>(type: "int", nullable: false),
                    Category = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Style = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StyleFull = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Story = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Alcohol = table.Column<float>(type: "real", nullable: false),
                    Ibu = table.Column<int>(type: "int", nullable: false),
                    Blg = table.Column<float>(type: "real", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Batches", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Galleries",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BatchId = table.Column<int>(type: "int", nullable: false),
                    Bottle = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Profile = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Label = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Opening = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Galleries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Galleries_Batches_BatchId",
                        column: x => x.BatchId,
                        principalTable: "Batches",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProcessData",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BatchId = table.Column<int>(type: "int", nullable: false),
                    FinalVolume = table.Column<float>(type: "real", nullable: true),
                    FinalBottles = table.Column<int>(type: "int", nullable: true),
                    WastedBottles = table.Column<int>(type: "int", nullable: true),
                    MashingTime = table.Column<float>(type: "real", nullable: true),
                    FiltrationTime = table.Column<float>(type: "real", nullable: true),
                    HoppingTime = table.Column<float>(type: "real", nullable: true),
                    PrimaryFermentationTime = table.Column<float>(type: "real", nullable: true),
                    SecondaryFermentationTime = table.Column<float>(type: "real", nullable: true),
                    BottlingTime = table.Column<float>(type: "real", nullable: true),
                    LabelingTime = table.Column<float>(type: "real", nullable: true),
                    BrewingDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    PrimaryFermentationDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    SecondaryFermentationDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    BottlingDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    OpenningDate = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcessData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProcessData_Batches_BatchId",
                        column: x => x.BatchId,
                        principalTable: "Batches",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Galleries_BatchId",
                table: "Galleries",
                column: "BatchId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProcessData_BatchId",
                table: "ProcessData",
                column: "BatchId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Galleries");

            migrationBuilder.DropTable(
                name: "ProcessData");

            migrationBuilder.DropTable(
                name: "Batches");
        }
    }
}

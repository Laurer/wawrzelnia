using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.IO;
using System.Reflection;

namespace Wawrzelnia.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            SetupLogger();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host
            .CreateDefaultBuilder(args)
            .UseSerilog()
            .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

        private static void SetupLogger()
        {
            var path = Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().Location).LocalPath);
            var logPath = Path.Combine(path, "Logs");
            Directory.CreateDirectory(logPath);
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .WriteTo.Console()
                .WriteTo.File(Path.Combine(logPath, $"log_{DateTime.Now.ToString("yyyyMMdd_HHmm")}.txt"))
                .CreateLogger();
        }
    }
}

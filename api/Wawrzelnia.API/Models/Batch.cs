﻿using System.ComponentModel.DataAnnotations;

namespace Wawrzelnia.API.Models
{
    public class Batch
    {
        /// <summary>
        /// Ordinal number of the batch in the brewery
        /// </summary>
        [Required] public int BatchNo { get; set; }
        /// <summary>
        /// Name of the batch
        /// </summary>
        [Required] public string Name { get; set; }
        /// <summary>
        /// Current status of the brewing process
        /// </summary>
        [Required] public BrewStage Stage { get; set; }

        /// <summary>
        /// The main category of the beer (based on BJCP classification) 
        /// </summary>
        [Required] public string Category { get; set; }
        /// <summary>
        /// The main style of beer - subcategory, like "AIPA", without any specific key features
        /// </summary>
        [Required] public string Style { get; set; }
        /// <summary>
        /// Full style of the beer, including additional aromas, flavours, etc.
        /// </summary>
        [Required] public string StyleFull { get; set; }

        /// <summary>
        /// Description of the beer, as per the bottle label
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Detailed background for the time and/or reasons, when the batch was brewed
        /// </summary>
        public string Story { get; set; }

        /// <summary>
        /// Estimated percentage of alcohol
        /// </summary>
        public float Alcohol { get; set; }
        /// <summary>
        /// Estimated IBU
        /// </summary>
        public int Ibu { get; set; }
        /// <summary>
        /// Initial gravity (before fermentation)
        /// </summary>
        public float Blg { get; set; }

        /// <summary>
        /// Values and statistics of the brewing process
        /// </summary>
        [Required] public ProcessData Process { get; set; }
        /// <summary>
        /// Set of photos and resources for the batch
        /// </summary>
        [Required] public Gallery Gallery { get; set; }

        public override string ToString() => $"Batch {BatchNo}: {Name}";
    }
}

﻿namespace Wawrzelnia.API.Models
{
    public class Gallery
    {
        /// <summary>
        /// Regular photo, presenting the bottle on a neutral, grey background
        /// </summary>
        public string Bottle { get; set; }
        /// <summary>
        /// Narrow vertical photo conatining just the bottle, with transparent background
        /// </summary>
        public string Profile { get; set; }
        /// <summary>
        /// Rectangular photo of the bottle's label
        /// </summary>
        public string Label { get; set; }
        /// <summary>
        /// Photo presenting the beer in a Teku glass
        /// </summary>
        public string Opening { get; set; }
    }
}

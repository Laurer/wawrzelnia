﻿using System;

namespace Wawrzelnia.API.Models
{
    public class ProcessData
    {
        /// <summary>
        /// Final ammount of the beer, when bottling (or kegging), in liters
        /// </summary>
        public float? FinalVolume { get; set; }
        /// <summary>
        /// Final count of full bottles (including test ones)
        /// </summary>
        public int? FinalBottles { get; set; }
        /// <summary>
        /// Final count of filled kegs
        /// </summary>
        public int? FinalKegs { get; set; }
        /// <summary>
        /// Count of bottles, which went to waste (exploded or needed to be poured out
        /// </summary>
        public int? WastedBottles { get; set; }

        /// <summary>
        /// Duration of mashing for this batch, in hours
        /// </summary>
        public float? MashingTime { get; set; }
        /// <summary>
        /// Duration of filtration for this batch, in hours
        /// </summary>
        public float? FiltrationTime { get; set; }
        /// <summary>
        /// Duration of boiling for this batch, in hours
        /// </summary>
        public float? HoppingTime { get; set; }
        /// <summary>
        /// Duration of primary fermentation, in days
        /// </summary>
        public float? PrimaryFermentationTime { get; set; }
        /// <summary>
        /// Duration of secondary fermentation, in days
        /// </summary>
        public float? SecondaryFermentationTime { get; set; }
        /// <summary>
        /// Duration of bottling (and/or kegging) for this batch
        /// </summary>
        public float? BottlingTime { get; set; }
        /// <summary>
        /// Duration of labeling for this batch
        /// </summary>
        public float? LabelingTime { get; set; }

        /// <summary>
        /// Date, when the batch was brewed
        /// </summary>
        public DateTime? BrewingDate { get; set; }
        /// <summary>
        /// Date, when the yeast was added and primary fermentation begun
        /// </summary>
        public DateTime? PrimaryFermentationDate { get; set; }
        /// <summary>
        /// Date, when the secondary fermentation was started
        /// </summary>
        public DateTime? SecondaryFermentationDate { get; set; }
        /// <summary>
        /// Date, when the beer was bottled (or kegged)
        /// </summary>
        public DateTime? BottlingDate { get; set; }
        /// <summary>
        /// Date, when the batch finished maturing and was ready to be drunk
        /// </summary>
        public DateTime? OpenningDate { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace Wawrzelnia.API.Models
{
    public class ContactForm
    {
        /// <summary>
        /// Name of the sender
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// E-mail address to send the response to
        /// </summary>
        [Required]
        public string ReturnAddress { get; set; }
        /// <summary>
        /// Content of the message
        /// </summary>
        [Required] 
        public string Message { get; set; }
    }
}

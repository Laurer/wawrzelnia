﻿namespace Wawrzelnia.API.Models
{
    /// <summary>
    /// Status of a batch in the brewing process
    /// </summary>
    public enum BrewStage
    {
        /// <summary>
        /// Recipe is being prepared
        /// </summary>
        Design = 1,
        /// <summary>
        /// Batch is being prepared (brewed or fermented)
        /// </summary>
        Brewing = 2,
        /// <summary>
        /// The batch is already in bottle (or keg), not ready to be drank yet
        /// </summary>
        Maturing = 3,
        /// <summary>
        /// The batch is ready
        /// </summary>
        Opened = 4,
        /// <summary>
        /// No more bottles are left
        /// </summary>
        Finished = 5
    }
}

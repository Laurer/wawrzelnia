﻿namespace Wawrzelnia.API.Models
{
    public class VersionInfo
    {
        public VersionInfo(string assemblyInfo)
        {
            AssemblyInfo = assemblyInfo;
        }

        /// <summary>
        /// Version of the interface
        /// </summary>
        public string ApiVersion { get; } = "v1";
        /// <summary>
        /// Version of the assembly
        /// </summary>
        public string AssemblyInfo { get; set; }
    }
}

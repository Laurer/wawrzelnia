﻿namespace Wawrzelnia.API.Models
{
    public class Stats
    {
        /// <summary>
        /// Count of batches, at all stages (both in progress and finished)
        /// </summary>
        public int BatchesCount { get; set; }

        /// <summary>
        /// Average alcohol of all batches
        /// </summary>
        public float AverageAlcohol { get; set; }
        /// <summary>
        /// Average IBU of all batches
        /// </summary>
        public float AverageIbu { get; set; }
        /// <summary>
        /// Average initial gravity
        /// </summary>
        public float AverageBlg { get; set; }

        /// <summary>
        /// Total ammount of bottled beers, in liters
        /// </summary>
        public float TotalLitres { get; set; }
        /// <summary>
        /// Total count of filled bottles (excluding wasted ones)
        /// </summary>
        public int TotalBottles { get; set; }
        /// <summary>
        /// Total count of filled kegs
        /// </summary>
        public int TotalKegs { get; set; }
        /// <summary>
        /// Total count of wasted bottles (exploded or poured out)
        /// </summary>
        public int TotalWastedBottles { get; set; }


        /// <summary>
        /// Total time spent on mashing, in hours
        /// </summary>
        public float MashingHours { get; set; }
        /// <summary>
        /// Total time spent on boiling (hopping), in hours
        /// </summary>
        public float HoppingHours { get; set; }
        /// <summary>
        /// Total time the beers was in primary fermentation, in days
        /// </summary>
        public int PrimaryFermentationDays { get; set; }
        /// <summary>
        /// Total time the beers was in secondary fermentation, in days
        /// </summary>
        public int SecondaryFermentationDays { get; set; }
        /// <summary>
        /// Total time spent on bottling, in hours
        /// </summary>
        public float BottlingHours { get; set; }
        /// <summary>
        /// Total time the beers were left for maturing, in days
        /// </summary>
        public int MaturingDays { get; set; }
    }
}

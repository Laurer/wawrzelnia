﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Text.Json;
using System.Threading.Tasks;
using Wawrzelnia.API.Models;

namespace Wawrzelnia.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ContactController : ControllerBase
    {
        private const int COOLDOWN_MINUTES = 120;
        private static DateTime _lastMailTime;

        private readonly ILogger<ContactController> _logger;
        private readonly IContactMailSender _mailSender;

        public ContactController(ILogger<ContactController> logger, IContactMailSender mailSender)
        {
            _logger = logger;
            _mailSender = mailSender;
        }

        /// <summary>
        /// Triggers sending a contact form e-mail to the Wawrzelnia's address, with the provided details
        /// </summary>
        /// <param name="contactForm"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> SendFeedback([FromBody] ContactForm contactForm)
        {
            if ((DateTime.UtcNow - _lastMailTime).TotalMinutes < COOLDOWN_MINUTES)
            {
                _logger.LogInformation($"[CONTACT]: Rejected send request - less than {COOLDOWN_MINUTES}min since the last message elapsed. The message was: {JsonSerializer.Serialize(contactForm)}");
                return BadRequest();
            }

            _logger.LogInformation($"[CONTACT]: {JsonSerializer.Serialize(contactForm)}");
            await _mailSender.SendAsync(contactForm);
            _lastMailTime = DateTime.UtcNow;
            return NoContent();
        }
    }
}

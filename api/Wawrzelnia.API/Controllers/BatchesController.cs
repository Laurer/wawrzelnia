﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Wawrzelnia.API.Models;
using Wawrzelnia.Database;

namespace Wawrzelnia.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class BatchesController : ControllerBase
    {
        private readonly DataContext _dbContext;
        private readonly IMapper _mapper;

        public BatchesController(DataContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        /// <summary>
        /// Returns all details of all batches
        /// </summary>
        /// <returns>A list of batches</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Batch>), StatusCodes.Status200OK)]
        public ActionResult<IEnumerable<Batch>> Get()
        {
            var allBatches = _dbContext.Batches
                .Include(_ => _.Process)
                .Include(_ => _.Gallery)
                .OrderBy(_ => _.BatchNo)
                .Select(_ => _mapper.Map<Batch>(_))
                .ToList();
            return Ok(allBatches);
        }

        /// <summary>
        /// Calculates current statistics of the brewery (average brew values, total volumes and times, etc.)
        /// </summary>
        /// <returns></returns>
        [HttpGet("/batches/stats")]
        [ProducesResponseType(typeof(Stats), StatusCodes.Status200OK)]
        public ActionResult<Stats> GetStats()
        {
            var batches = _dbContext.Batches
                .Include(_ => _.Process)
                .Where(_ => _.Stage >= Database.Models.BrewStage.Opened);
            var stats = new Stats()
            {
                BatchesCount = batches.Count(),
                TotalLitres = batches.Sum(_ => _.Process.FinalVolume.Value),
                TotalWastedBottles = batches.Sum(_ => _.Process.WastedBottles.Value),
                TotalBottles = batches.Sum(_ => _.Process.FinalBottles.Value),
                MashingHours = CalculateStat(batches, _ => _.Process.MashingTime),
                HoppingHours = CalculateStat(batches, _ => _.Process.HoppingTime),
                PrimaryFermentationDays = CalculateStat(batches, _ => (float)(_.Process.SecondaryFermentationDate - _.Process.PrimaryFermentationDate).Value.TotalDays),
                SecondaryFermentationDays = CalculateStat(batches, _ => (float)(_.Process.BottlingDate - _.Process.SecondaryFermentationDate).Value.TotalDays),
                BottlingHours = CalculateStat(batches, _ => _.Process.BottlingTime),
                MaturingDays = CalculateStat(batches, _ => (float)(_.Process.OpenningDate - _.Process.BottlingDate).Value.TotalDays / 7),
                AverageAlcohol = batches.Average(_ => _.Alcohol),
                AverageIbu = (float)batches.Average(_ => _.Ibu),
                AverageBlg = batches.Average(_ => _.Blg)
            };
            return Ok(stats);
        }

        private static int CalculateStat(IEnumerable<Database.Models.Batch> batches, Func<Database.Models.Batch, float?> predicate)
        {
            var average = batches.Select(predicate).Where(_ => _ != null).Average();
            return average.HasValue ? (int)(average.Value * batches.Count()) : -1;
        }
    }
}

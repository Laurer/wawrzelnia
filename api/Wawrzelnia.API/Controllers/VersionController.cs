﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Reflection;
using Wawrzelnia.API.Models;

namespace Wawrzelnia.API.Controllers
{
    /// <summary>
    /// Retunrs detailed information about the system's version
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class VersionController
    {
        [HttpGet]
        [ProducesResponseType(typeof(VersionInfo), StatusCodes.Status200OK)]
        public ActionResult<VersionInfo> Get() => new VersionInfo(FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion);
    }
}

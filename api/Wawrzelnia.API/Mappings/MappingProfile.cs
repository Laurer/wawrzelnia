﻿using AutoMapper;

namespace Wawrzelnia.API.Mappings
{
    internal class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Models.Batch, Database.Models.Batch>();
            CreateMap<Models.Gallery, Database.Models.Gallery>();
            CreateMap<Models.ProcessData, Database.Models.ProcessData>();

            CreateMap<Database.Models.Batch, Models.Batch>();
            CreateMap<Database.Models.Gallery, Models.Gallery>();
            CreateMap<Database.Models.ProcessData, Models.ProcessData>();
        }
    }
}

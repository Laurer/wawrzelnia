﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;
using System.Threading.Tasks;
using Wawrzelnia.API.Models;
using Wawrzelnia.API.Settings;

namespace Wawrzelnia.API
{
    internal class ContactMailSender : IContactMailSender
    {
        private readonly ILogger _logger;
        private MailSettings _mailSettings;

        public ContactMailSender(IOptions<MailSettings> mailSettings, ILogger<ContactMailSender> logger)
        {
            _mailSettings = mailSettings.Value;
            _logger = logger;
        }

        public async Task SendAsync(ContactForm contactForm)
        {
            try
            {
                var mimeMessage = new MimeMessage();
                mimeMessage.From.Add(new MailboxAddress(contactForm.Name, contactForm.ReturnAddress));
                mimeMessage.To.Add(new MailboxAddress(_mailSettings.ToName, _mailSettings.ToAddress));
                mimeMessage.Subject = $"Wawrzelne pytanie od {contactForm.Name}";
                mimeMessage.Body = new TextPart("plain") { Text = contactForm.Message };

                using (var client = new SmtpClient())
                {
                    await client.ConnectAsync(_mailSettings.Server, _mailSettings.Port, true);
                    await client.AuthenticateAsync(_mailSettings.User, _mailSettings.Password);
                    await client.SendAsync(mimeMessage);
                    await client.DisconnectAsync(true);
                }

            }
            catch(AuthenticationException ex)
            {
                _logger.LogError($"Failed to authenticate with this data: {_mailSettings}\nMessage: {ex.Message}");
                throw;
            }
            catch
            {
                // TODO: handle exception
                throw;
            }
        }
    }
}

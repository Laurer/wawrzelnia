﻿using System.Threading.Tasks;
using Wawrzelnia.API.Models;

namespace Wawrzelnia.API
{
    public interface IContactMailSender
    {
        Task SendAsync(ContactForm contactForm);
    }
}

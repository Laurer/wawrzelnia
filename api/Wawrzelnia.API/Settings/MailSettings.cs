﻿namespace Wawrzelnia.API.Settings
{
    internal class MailSettings
    {
        public string Server { get; set; }
        public int Port { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string ToAddress { get; set; }
        public string ToName { get; set; }

        public override string ToString() => $"Credentials: {User}/{Password}, Server: {Server}:{Port}, Recipient: {ToName}({ToAddress})";
    }
}

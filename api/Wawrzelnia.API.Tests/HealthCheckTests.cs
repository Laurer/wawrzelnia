using FluentAssertions;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Wawrzelnia.API.Tests
{
    public class HealthCheckTests : ApiTestsBase
    {
        [Test]
        public async Task DbAccessible_ReturnsHealthy()
        {
            var response = await _client.GetAsync("/health");
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            content.Should().Be("Healthy");
        }

        [Test]
        public async Task DbUnavailable_ReturnsDegraded()
        {
            var client = CreateApiClientSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=DbThatDoesntExist;");
            var response = await client.GetAsync("/health");
            response.StatusCode.Should().Be(System.Net.HttpStatusCode.ServiceUnavailable);
            var content = await response.Content.ReadAsStringAsync();
            content.Should().Be("Unhealthy");
        }

        private static HttpClient CreateApiClientSqlServer(string connectionString)
        {
            var factory = new WebApplicationFactory<Startup>()
                .WithWebHostBuilder(builder =>
                {
                    builder.ConfigureAppConfiguration((context, configBuilder) =>
                    {
                        configBuilder.AddInMemoryCollection(
                            new Dictionary<string, string>
                            {
                                ["ConnectionStrings:Default"] = connectionString
                            });
                    });
                });
            return factory.CreateClient();
        }
    }
}
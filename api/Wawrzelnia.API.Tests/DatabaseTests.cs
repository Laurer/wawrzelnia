﻿using FluentAssertions;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.IO;
using System.Threading.Tasks;
using Wawrzelnia.Database;

namespace Wawrzelnia.API.Tests
{
    public class DatabaseTests
    {
        private static string ConnectionString => @"Data Source=Snapshots/wawrzelnia.sqlite";

        [Test]
        public async Task DbConnectsCorrectly()
        {
            var connection = new SqliteConnection(ConnectionString);
            connection.Open();
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseSqlite(connection)
                .Options;
            var context = new DataContext(options);
            context.Database.EnsureCreated();

            var batch = await context.Batches.FirstAsync();
            batch.Name.Should().Be("Pierwowar");
        }
    }
}
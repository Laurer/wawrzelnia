﻿using FluentAssertions;
using Newtonsoft.Json;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wawrzelnia.API.Models;

namespace Wawrzelnia.API.Tests
{
    public class BatchesTests : ApiTestsBase
    {
        [Test]
        public async Task GetAll_ReturnsFullList()
        {
            var response = await _client.GetAsync("/batches");
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            var batches = JsonConvert.DeserializeObject<IEnumerable<Batch>>(content);
            batches.Should().HaveCount(18);
        }

        [Test]
        public async Task GetAll_ReturnsAllDetails()
        {
            var response = await _client.GetAsync("/batches");
            response.EnsureSuccessStatusCode();
            var content = await response.Content.ReadAsStringAsync();
            var first = JsonConvert.DeserializeObject<IEnumerable<Batch>>(content).First();
            first.BatchNo.Should().Be(1);
            first.Name.Should().Be("Pierwowar");
            first.Stage.Should().Be(BrewStage.Finished);
            first.Category.Should().Be("IPA");
            first.Style.Should().Be("Black IPA");
            first.StyleFull.Should().Be("New Zealand Black IPA");
            first.Description.Should().NotBeNullOrEmpty();
            first.Story.Should().NotBeNullOrEmpty();
            first.Alcohol.Should().Be(7);
            first.Ibu.Should().Be(105);
            first.Blg.Should().Be(15);

            first.Process.Should().NotBeNull();
            first.Process.FinalVolume.Should().Be(18.5f);
            first.Process.FinalBottles.Should().Be(37);
            first.Process.WastedBottles.Should().Be(0);

            first.Gallery.Should().NotBeNull();
            first.Gallery.Bottle.Should().NotBeNull();
            first.Gallery.Profile.Should().NotBeNull();
            first.Gallery.Label.Should().NotBeNull();
        }
    }
}
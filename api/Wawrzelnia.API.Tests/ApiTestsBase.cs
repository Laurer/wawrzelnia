﻿using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System.Net.Http;
using Wawrzelnia.Database;

namespace Wawrzelnia.API.Tests
{
    public abstract class ApiTestsBase
    {
        protected readonly HttpClient _client;

        private static string ConnectionString => @"Data Source=Snapshots/wawrzelnia.sqlite";

        public ApiTestsBase()
        {
            _client = CreateApiClientSqlite();
        }

        protected HttpClient CreateApiClientSqlite()
        {
            var factory = new WebApplicationFactory<Startup>()
                .WithWebHostBuilder(builder =>
                {
                    builder.ConfigureTestServices(services =>
                    {
                        // Remove the original SQL Server provider
                        var descriptor = services.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<DataContext>));
                        if (descriptor != null)
                        {
                            services.Remove(descriptor);
                        }

                        // Add SQLite provider for tests
                        services.AddDbContext<DataContext>(options => options.UseSqlite(ConnectionString));
                    });
                });
            return factory.CreateClient();
        }
    }
}